


// tinh = (...moreInfor) => {

//     let toan = document.getElementById("inpToan").value * 1;
//     let ly = document.getElementById("inpLy").value * 1;
//     let hoa = document.getElementById("inpHoa").value * 1;
//     let van = document.getElementById("inpVan").value * 1;
//     let su = document.getElementById("inpSu").value * 1;
//     let dia = document.getElementById("inpDia").value * 1;
//     let english = document.getElementById("inpEnglish").value * 1;
//     var diemTB1 = (toan + ly + hoa) / 3;
//     var diemTB2 = (van + dia + su + english) / 4;
//     document.getElementById("tbKhoi1").innerHTML = diemTB1
//     document.getElementById("tbKhoi2").innerHTML = diemTB2

// }
// tinh()
let tinhDiemTrungBinh = (...rest) => {
    let diemTong = 0;
    let diemTB = 0;

    for (let i = 0; i < rest.length; i++) {
        diemTong += rest[i];
        diemTB = diemTong / rest.length;
    }
    return diemTB.toFixed(2);
};
let tinhDiemK1 = () => {
    let diemToan = document.querySelector("#inpToan").value * 1;
    let diemLy = document.querySelector("#inpLy").value * 1;
    let diemHoa = document.querySelector("#inpHoa").value * 1;
    let result = tinhDiemTrungBinh(diemToan, diemLy, diemHoa);

    document.querySelector("#tbKhoi1").innerHTML = result;
};

let tinhDiemK2 = () => {
    let diemVan = document.querySelector("#inpVan").value * 1;
    let diemSu = document.querySelector("#inpSu").value * 1;
    let diemDia = document.querySelector("#inpDia").value * 1;
    let diemEnglish = document.querySelector("#inpEnglish").value * 1;
    let result = tinhDiemTrungBinh(diemVan, diemSu, diemDia, diemEnglish);

    document.querySelector("#tbKhoi2").innerHTML = result;
};
